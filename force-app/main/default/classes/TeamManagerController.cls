/**
 * Name: TeamManagerController 
 * Description: Controller for the Team Manager Feature
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
global with sharing class TeamManagerController {
    
    private static String TEAM_MANAGER_FEATURE_PATH = 'ACE.Extensions.TeamManager';
    
    public static T1C_FR.Feature teamManagerFeature
    {
        get
        {
            if (teamManagerFeature == null)
            {
                teamManagerFeature = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), TEAM_MANAGER_FEATURE_PATH);
            }
            return teamManagerFeature;
        }
        private set;
    }
    
    public TeamManagerController()
    {
    }
    
    ////  ==================================================================
    ////  Method: getConfig
    ////  Description: Gets the configuration for the Team Manager form
    ////  The input is not needed functionally, but an input is required in the signature
    ////  ==================================================================
    WebService static string getConfig(String dummyOptions)
    {
        // deserialize the options, then pass into team Manager Config.
        Map<String, String> optionsMap = (Map<String, String>)JSON.deserialize(dummyOptions, Map<String, String>.class);
        
        return JSON.serialize(new TeamManagerConfig(teamManagerFeature, optionsMap));

        // return JSON.serialize(new TeamManagerConfig( teamManagerFeature ) );
    }  
    
    
    public class TeamManagerConfig
    {
        public string headerTitle;
        public string managingRoles;
        public List<TeamManagerGridField> gridColumns = new List<TeamManagerGridField>();
        public Boolean isAdmin;
        public String curEmployeeId;
        public TeamManagerGridField employeeLookup;
        public String noTeamMessage;
        public String multiDeleteMessage;
        public String saveMessage;
        public String saveMessageLabel;
        public String filterPlaceHolder;
        public String userName; // use if logged in OBO someone. 
        
        public TeamManagerConfig(T1C_FR.Feature inputFeature,  Map<String, String> options)
        {
            
            this.employeeLookup = new TeamManagerGridField( AFRUtil.getSubfeature(teamManagerFeature, 'EmployeeLookup') );
            
            //Configure defaults
            this.headerTitle = AFRUtil.getAttributeValue(inputFeature,'HeaderLabel','Team Manager');
            //this.headerTitle = String.isBlank( headerTitle ) ? 'Team Manager' : headerTitle;
            this.managingRoles = AFRUtil.getAttributeValue(inputFeature,'ManagerRoles','Manager');
            //this.managingRoles = String.isBlank( managingRoles ) ? 'Manager' : managingRoles;
            this.noTeamMessage = AFRUtil.getAttributeValue(inputFeature,'NoTeamMessage','You do not belong to any teams. Team Manager will not load');
            this.multiDeleteMessage = AFRUtil.getAttributeValue(inputFeature,'MultiDeleteMessage', 'Delete Selected Members? You will still be able to undo your changes');
            this.saveMessage = AFRUtil.getAttributeValue(inputFeature,'SaveMessage', 'Save Changes?');
            this.saveMessageLabel = AFRUtil.getAttributeValue(inputFeature,'SaveSuccessMessage', 'Team Members saved Successfully');
            this.filterPlaceHolder = AFRUtil.getAttributeValue(inputFeature,'FilterPlaceHolder', 'Search Teams');
            
            //Column Features
            T1C_FR.Feature columnsFeature = AFRUtil.getSubfeature(teamManagerFeature, 'Layout.GridColumns');
            
            for ( T1C_FR.Feature colFeature : columnsFeature.SubFeatures )
			{
			    String fieldName = AFRUtil.getAttributeValue(colFeature,'DataField');
			    this.gridColumns.add( new TeamManagerGridField(colFeature) );
			}
			
            // if a userId is an option, use that.
            Id userId;
            if (options.get('currentUserId') != null && options.get('currentUserId') != '')
            {
                userId = Id.valueOf(options.get('currentUserId'));
                this.userName = [SELECT Name FROM User WHERE Id =: userId].Name;
            }
            else
            {
                userId = UserInfo.getUserId();
                this.userName = null;
            }

			
			//Find the Employee this user belongs to. Should only pull ONE Employee
	        List<T1C_Base__Employee__c> employeeList =[SELECT Id, 
                                                           T1C_Base__User__c, 
                                                           Tier1_Team_Manager_Support__c 
                                                       FROM T1C_Base__Employee__c 
                                                       WHERE T1C_Base__User__c = :userId];
	        
	        this.curEmployeeId = employeeList[0].Id;
			this.isAdmin = employeeList[0].Tier1_Team_Manager_Support__c;
			
			
        }
    }
    
    
    /*==================================================================
      Class: TeamManagerGridField
      Description: Contains the configuration for the Team Manager form grid
      ==================================================================*/
    public class TeamManagerGridField
    {
        
        public string id;
        public string fieldType;
        public string width;
        public string label;
        public Integer order;
        public string defaultValue;
        public string defaultId;
        public boolean visible;
        public String sObjectName;
        public String dataField;
        public list<string> picklistOptions;
        public Object additionalConfig;
        public String parentSObject;
        public String parentSObjectField;
        public Boolean disabled;
        public String placeHolder;
        
        public TeamManagerGridField(T1C_FR.Feature inputFeature)
        {
            this.id                 = AFRUtil.getAttributeValue(inputFeature,'Id',inputFeature.getName() );
            this.fieldType          = AFRUtil.getAttributeValue(inputFeature,'FieldType','String');
            this.width              = AFRUtil.getAttributeValue(inputFeature,'Width','100');
            this.label              = AFRUtil.getAttributeValue(inputFeature,'Label');
            this.order              = AFRUtil.getIntegerAttributeValue(inputFeature,'Order',-1);
            this.parentSObjectField = AFRUtil.getAttributeValue(inputFeature,'ParentSObjectField'); 
            this.sObjectName        = AFRUtil.getAttributeValue(inputFeature,'SObjectName'); 
            this.dataField          = AFRUtil.getAttributeValue(inputFeature,'DataField');
            this.visible            = AFRUtil.getBooleanAttributeValue(inputFeature,'Visible',false);
            this.disabled           = AFRUtil.getBooleanAttributeValue(inputFeature,'Disabled',false);
            this.placeHolder        = AFRUtil.getAttributeValue(inputFeature,'PlaceHolder');
            this.defaultValue = AFRUtil.getAttributeValue(inputFeature, 'DefaultValue' ,this.defaultValue);
            
            //set up the additionalConfig properties to have the autocomplete to work
            if(this.fieldType == 'LOOKUP')
            {
                SObjectUtil.DynamicUILookupConfig o = new SObjectUtil.DynamicUILookupConfig( AFRUtil.getSubfeature(inputFeature, 'FieldConfig') );
                this.additionalConfig = new SObjectUtil.DynamicUILookupConfig();
                this.additionalConfig = o;
                
                
            }//field type LOOKUP
            else if(this.fieldType == 'PICKLIST')
            {
               SObjectUtil.DynamicUIPicklistConfig o = new SObjectUtil.DynamicUIPicklistConfig();
               list<String> pickListValues = o.getPicklistValues(this.sObjectName, this.dataField);

               list<PickListItem> dataProv = new list<PickListItem>();
               for(String item : pickListValues)
               {
                   dataProv.add( new PickListItem(item) );
               }

               o.dataProvider = dataProv;
               this.defaultValue = o.defaultValue;
               //instantiate additonalConfig as DynamicUIPicklistConfig
               this.additionalConfig = new SObjectUtil.DynamicUIPicklistConfig();
               this.additionalConfig = o;
            }
            
        }
    }
    
    ////  ==================================================================
    ////  Method: getTeamRecords
    ////  Description: retrieves Team data relating to this user
    ////  ==================================================================
    WebService static string getTeamRecords(string teamRecordDetails)
    {
        // userId from options
        Map<String, String> options = (Map<String, String>)JSON.deserialize(teamRecordDetails, Map<String, String>.class);// if a userId is an option, use that.
        Id userId;
        if (options.get('currentUserId') != null && options.get('currentUserId') != '')
        {
            userId = Id.valueOf(options.get('currentUserId'));
        }
        else
        {
            userId = UserInfo.getUserId();
        }
        
        Set<String> queryCols;
        //Find the Employee this user belongs to. Should only pull ONE Employee
        List<T1C_Base__Employee__c> employeeList =[SELECT Id, T1C_Base__User__c, Tier1_Team_Manager_Support__c FROM T1C_Base__Employee__c WHERE T1C_Base__User__c = :userId];
        
        Id employee = employeeList[0].Id;
        
        //Support member should be able to see all teams
        Boolean seeAllTeams = employeeList[0].Tier1_Team_Manager_Support__c;
        
        //Find the teams where this employee is a member
        List<T1C_Base__Team_Member__c> teamList = [SELECT Id, 
											        T1C_Base__Team__c, 
											        T1C_Base__Employee__c, 
											        T1C_Base__Role__c 
										          FROM T1C_Base__Team_Member__c 
										          WHERE T1C_Base__Employee__c = :employee AND
										          T1C_Base__Inactive__c = false AND 
                                                  T1C_Base__Team__c != null];
        
        if( teamList.size() < 1 && !seeAllTeams)
        {
            throw new teamManagerException('You do not belong to any teams');
        }
        
        Set<Id> teamIds = new Set<Id>();
        List<T1C_Base__Team__c> teamsList; //For storing the actual Team objects. Will be queried later on
        
        for(T1C_Base__Team_Member__c curTeam : teamList)
        {
            teamIds.add( curTeam.T1C_Base__Team__c );
        }
        
        //Arrange the query columns. The ones that are hard-coded are mandatory fields, but we need settings for them so we add it as a set such that no dupes arise
        queryCols = new Set<String>{ 'Id', 'T1C_Base__Team__c', 'T1C_Base__Team__r.Name', 'T1C_Base__Employee__c', 'T1C_Base__Role__c', 'T1C_Base__Inactive__c' };
          
        T1C_FR.Feature columnsFeature = AFRUtil.getSubfeature(teamManagerFeature, 'Layout.GridColumns');
        
        for ( T1C_FR.Feature colFeature : columnsFeature.SubFeatures )
        {
            String fieldName = AFRUtil.getAttributeValue(colFeature,'DataField');
            if ( !String.isBlank(fieldName) ){queryCols.add( fieldName );}
        }

        //Now Find the full Team Members list so we can display them in the form. Order by the Team (T1C_Base__Team__c). It's the data here that goes on the form
        String cols = String.join( new List<String>(queryCols),',');
        cols = cols.removeEnd(',/s*'); 
        
        //Get AFR Fields for Header with mandatory fields
        String queryString = 'SELECT ' + cols + ' FROM T1C_Base__Team_Member__c' ;
        
        if(!seeAllTeams)
        {
            queryString = queryString + ' WHERE T1C_Base__Team__c IN :teamIds AND T1C_Base__Inactive__c = false ';
            teamsList = [Select Id, Name FROM T1C_Base__Team__c WHERE Id in :teamIds];
        } else 
        {
            //If the user is a support manager, query for all the teams
            queryString = queryString + ' WHERE T1C_Base__Inactive__c = false AND T1C_Base__Team__c != null';
            teamsList = [Select Id, Name FROM T1C_Base__Team__c WHERE Active__c = true];
        }
        
        system.debug(queryString);
        
        List<Sobject> membersList = Database.query(queryString);
        
        return JSON.serialize( new TeamReturnData( (List<T1C_Base__Team_Member__c>)membersList,teamsList) );
        
    }
    
    public class teamManagerException extends Exception
    {
    }
    
    WebService static void saveTeamRecords(string teamRecordDetails)
    {
        //They're not allowed to make teams, so we can assume only Team members are being updated
        List<SObject> optionInputs = (List<SObject>) JSON.deserialize( teamRecordDetails, Type.forName('List<T1C_Base__Team_Member__c>') );
        map<String,String> teamsMap = new map<String,String>();  
        List<Id> deleteQueue = new List<Id>();
        
        for (SObject a : optionInputs)
	    {
	        //acntContRelType.
			//Check for dupes
			String identifier = a.get('T1C_Base__Employee__c') + '/' + a.get('T1C_Base__Team__c');
			String b = String.valueOf(  teamsMap.get( identifier )  );
			if( b != null)
			{ 
			    continue;
			}
			teamsMap.put( identifier, 'Unique' );
			system.debug('Processing team Member: ' + a);
			if( ((String) a.get('Id')).contains('new') )
			{
				system.debug('Removing Id ' + a.get('Id') );
				a.put('Id', null);
				a.put( 'Name',( (T1C_Base__Team_Member__c)a).T1C_Base__Employee__r.Name );
			}
			if(  Boolean.valueOf( a.get('T1C_Base__Inactive__c') )  )
			{//Pop from upsert queue and add to delete queue;
				system.debug('Adding to Delete Queue: ' + a.get('Id'));
				deleteQueue.add((Id)a.get('Id'));
			}
	    }
	    
	    T1C_Base.AceDMLUtilWithoutSharing.upsertRecords( optionInputs );
        T1C_Base.AceDMLUtilWithoutSharing.deleteRecords( deleteQueue );
        
        
    }
    
    public class TeamReturnData
    {
        public List<T1C_Base__Team_Member__c> membersList;
        public List<T1C_Base__Team__c> teamsList;
        
        public TeamReturnData(List<T1C_Base__Team_Member__c> inMembers , List<T1C_Base__Team__c> inTeams)
        {
            this.membersList = inMembers;
            this.teamsList = inTeams;
        }
        
    }
    
    //Just a quick adapter class for later versions of ACEMenuButton
    private class PickListItem
    {
        public String id;
        public String label;
        public String data;
        
        public PickListItem(String item)
        {
            this.id = item;
            this.label = item;
            this.data = item;
        }
        
    }
}