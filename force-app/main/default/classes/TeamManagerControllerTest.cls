/**
 * Name: TeamManagerControllerTest
 * Description: Test class for the Controller of the Team Manager Feature
 *
 * Confidential & Proprietary, 2016 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
@isTest
private class TeamManagerControllerTest {

    static testMethod void testTeamManagerController() 
    {
        T1C_FR__Feature__c aceFeature     = new T1C_FR__Feature__c(Name = 'ACE', T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c extensions     = new T1C_FR__Feature__c(Name = 'Extensions', T1C_FR__Name__c = 'ACE.Extensions');
        T1C_FR__Feature__c teamManager    = new T1C_FR__Feature__c(Name = 'TeamManager', T1C_FR__Name__c = 'ACE.Extensions.TeamManager');
        T1C_FR__Feature__c layout         = new T1C_FR__Feature__c(Name = 'Layout', T1C_FR__Name__c = 'ACE.Extensions.TeamManager.Layout');
        T1C_FR__Feature__c gridColumns    = new T1C_FR__Feature__c(Name = 'GridColumns', T1C_FR__Name__c = 'ACE.Extensions.TeamManager.Layout.GridColumns');
        T1C_FR__Feature__c employeeCol    = new T1C_FR__Feature__c(Name = 'Employee', T1C_FR__Name__c = 'ACE.Extensions.TeamManager.Layout.GridColumns.Employee');
        T1C_FR__Feature__c fieldConfigCol = new T1C_FR__Feature__c(Name = 'FieldConfig', T1C_FR__Name__c = 'ACE.Extensions.TeamManager.Layout.GridColumns.Employee.FieldConfig');
        T1C_FR__Feature__c roleCol        = new T1C_FR__Feature__c(Name = 'Role', T1C_FR__Name__c = 'ACE.Extensions.TeamManager.Layout.GridColumns.Role');
        T1C_FR__Feature__c employeeLookup = new T1C_FR__Feature__c(Name = 'EmployeeLookup', T1C_FR__Name__c = 'ACE.Extensions.TeamManager.EmployeeLookup');
        
        upsert new list<T1C_FR__Feature__c>{aceFeature, extensions, teamManager, layout, fieldConfigCol, gridColumns, employeeCol, roleCol, employeeLookup};
        
        insert new list<T1C_FR__Attribute__c>
        {                                          
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamManager.Id, Name = 'FilterPlaceHolder', T1C_FR__Value__c = 'Search teams'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamManager.Id, Name = 'MultiDeleteMessage', T1C_FR__Value__c = 'The selected team members will be marked for deletion. You will still be able to undo your changes.'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamManager.Id, Name = 'NoTeamMessage', T1C_FR__Value__c = 'You are not a member of any team. Please request access by contacting a team manager.'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamManager.Id, Name = 'SaveMessage', T1C_FR__Value__c = 'Are you sure you want to save your changes?'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = teamManager.Id, Name = 'SaveSuccessMessage', T1C_FR__Value__c = 'The team changes have been successfully saved.'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeCol.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeCol.Id, Name = 'Disabled', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeCol.Id, Name = 'FieldType', T1C_FR__Value__c = 'LOOKUP'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeCol.Id, Name = 'Id', T1C_FR__Value__c = 'Employee'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeCol.Id, Name = 'Label', T1C_FR__Value__c = 'Team Member'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = fieldConfigCol.Id, Name = 'SearchFields', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = fieldConfigCol.Id, Name = 'sObjectName', T1C_FR__Value__c = 'Team Member'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleCol.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleCol.Id, Name = 'Disabled', T1C_FR__Value__c = 'FALSE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleCol.Id, Name = 'FieldType', T1C_FR__Value__c = 'PICKLIST'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleCol.Id, Name = 'Id', T1C_FR__Value__c = 'Role'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleCol.Id, Name = 'Label', T1C_FR__Value__c = 'Team Role'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleCol.Id, Name = 'SObjectName', T1C_FR__Value__c = 'T1C_Base__Team_Member__c'),
            
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeLookup.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__c.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeLookup.Id, Name = 'FieldType', T1C_FR__Value__c = 'STRING'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = employeeLookup.Id, Name = 'PlaceHolder', T1C_FR__Value__c = 'Add a new team member')//,
            
        };
        
        Profile sysProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].get(0);
        
        
        User testUser = new User(username ='Test1asddvxzcvzxcvQF@test.ASSDFASDFASDF.com',LastName='Test1@test.com', Email='Test1@test.com', Alias='T1', TimeZoneSidKey='GMT', LocaleSidKey='en_US', 
        EmailEncodingKey='ISO-8859-1', ProfileId=sysProfile.Id, LanguageLocaleKey='en_US');
        insert testUser;
        
        T1C_Base__Employee__c testUserEmp = new T1C_Base__Employee__c();
        testUserEmp.put('T1C_Base__User__c',testUser.Id);
        testUserEmp.put('T1C_Base__Last_Name__c','Test Person');
        insert testUserEmp;
        
        T1C_Base__Team__c testTeam = new T1C_Base__Team__c(Name='testTeam',Active__c=true);
            
        insert testTeam;
        
        system.runAs(testUser)
        {
            TeamManagerController cntl = new TeamManagerController(); 
            
            String config = TeamManagerController.getConfig('{}');
            
            //No Teams yet, this should throw an exception
            try
            {
                TeamManagerController.getTeamRecords('{}');
            }
            catch(TeamManagerController.teamManagerException te)
            {
                
            }
            
            T1C_Base__Team_Member__c newMem = new T1C_Base__Team_Member__c(Name='testUser',T1C_Base__Employee__c=testUserEmp.Id,T1C_Base__Role__c='Manager',T1C_Base__Team__c=testTeam.Id);
            
            insert newMem;

            try
            {
                TeamManagerController.TeamReturnData teamReturns = 
                (TeamManagerController.TeamReturnData)JSON.deserialize(TeamManagerController.getTeamRecords('{}'), Type.forName('TeamManagerController.TeamReturnData') );
                
                System.assert(teamReturns.membersList.size() == 1);
            }
            catch(TeamManagerController.teamManagerException te)
            {
                System.debug('getTeamRecords Failed!');
            }
            
            newMem.T1C_Base__Role__c = 'Member';
            newMem.T1C_Base__Inactive__c = true;
            
            List<sObject> teamMembers = new List<sObject>{newMem};
            TeamManagerController.saveTeamRecords( JSON.serialize(teamMembers) );
            
        }//Run as test User
        
    }//testTeamManagerController
}//private class TeamManagerControllerTest