//=============================================================================
/**
 * Name: TeamManagerForm.js
 * Description:
 *
 * Confidential & Proprietary, 2018 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
    var _super = ACE.ClassUtil.inherit(TeamManagerForm, ACE.AbstractDialog);
    var _this;
    var _SObjectList;
    var validationError = false;
    var mainWindow;
    var isAdmin; //Variable to determine if user has edit access to this team
    var isDelegateAdmin; //Used to determine if this user has edit rights to all teams
    var sideBar;
    var config;
    var editable;//Reference toggle for the slick grid for Managed teams only
    var i = 0;
    var appendedEmployee = {};//Keep a reference to the appended employee
    var teamMembers;
    var teamSet;//set of all teams. Used to store qury results
    var managedTeams;//subset of managed teams
    var unmanagedTeams;//subset of unmanaged teams
    var managedTeamDiv;
    var unmanagedTeamDiv;
    var selectedTeam;//Reference to currently select team ID
    var selectedDiv;//Reference to currently selected Div
    var employeeLookup;
    var selectedTeamName;
    var deletedQueue;
    var tempDeleteQueue;
    var isDelete;
    var savingToast;
    var currentUserId; // could be OBO.
    
    /**
    * Constructor
    */
    function TeamManagerForm()
    {
        
        this.parent = $('<div class="teamManagerWrapper" />').appendTo('body');
        _this = this;
        _super.constructor.call(this, 
        {
            target      : this.parent,
            width       : "100%",
            height      : "auto",//window.innerHeight,
            left        : "0px",
            dialogClass: "TeamManagerFormDialog",
            buttons: 
            [
                {
                    text   : "Save",
                    id     : "customQuickAddSaveBtn",
                    "class": "confirmButton el-btn-filled el-btn-lg",
                    click  : function()
                    {
                        window.setTimeout(function()
                        {
                            Slick.GlobalEditorLock.commitCurrentEdit();
                        },  0);//Save Grid edits if any
                        ACEConfirm.show
                        (
                            _this.config.saveMessage,
                            "Confirm",
                            [ "Ok::confirmButton","Cancel::cancelButton"],
                            function(result)
                            {
                                if(result == 'Ok')
                                {_this.save(false);}//else do nothing
                            }
                        );
                    }
                },
                {
                    text   : "Save and Close",
                    id     : "customQuickAddSaveCloseBtn",
                    "class": "confirmButton el-btn-filled el-btn-lg",
                    click  : function()
                    {
                        window.setTimeout(function()
                        {
                            Slick.GlobalEditorLock.commitCurrentEdit();
                        },  0);//Save Grid edits if any
                        ACEConfirm.show
                        (
                            _this.config.saveMessage,
                            "Confirm",
                            [ "Ok::confirmButton","Cancel::cancelButton"],
                            function(result)
                            {
                                if(result == 'Ok')
                                {_this.save(true);}//else do nothing
                            }
                        );
                    }
                },
                {
                    text   : "Cancel",
                    "class": "cancelButton el-btn-outline",
                    click  : function()
                    {
                        //$("#customQuickAddSaveBtn")[0].disabled = false;
                        _this.close();
                    }
                }
            ]
        });
        this.show();
    }//Constructor
    
    TeamManagerForm.prototype.close = function()
	{
	   //TODO: pop close dialog only when changes occur
		window.close();
	}
    
    TeamManagerForm.prototype.show = function()
    {
        _super.show.call(this);    
    };
    
    /**
     * @inheritDoc
     */
    TeamManagerForm.prototype.init = function()
    {
        //Init sforce connection
        //establish connection using session ID
        sforce.connection.sessionId = ACE.Salesforce.sessionId;
        _this.isDelete = false;
        _this.deletedQueue = [];
        _this.tempDeleteQueue = [];
        currentUserId = ACEUtil.getURLParameter('userId');

        var options = 
        {
            'currentUserId':currentUserId
        };
        
        sforce.apex.execute('TeamManagerController', 'getConfig', { "dummyOptions": JSON.stringify(options) },
        {
            onSuccess: function(result)
            {
                _this.config = JSON.parse(result);
                _this.getTeamMembers();
                
            },//onSuccess
            onFailure: function(error)
            {
                ACE.FaultHandlerDialog.show
                ({
                    title:   "Error",
                    message: "An Unexpected Error Occured, please send the details to your Administrator", 
                    error:   error.faultstring
                });
            }//onFailure
        });//sforce.apex.execute 

    };//TeamManagerForm.prototype.init = function()
    
    TeamManagerForm.prototype.getTeamMembers  = function()
    {
        //Init sforce connection
        //establish connection using session ID
        sforce.connection.sessionId = ACE.Salesforce.sessionId;

        var options = 
        {
            'currentUserId':currentUserId
        };
        
        sforce.apex.execute('TeamManagerController', 'getTeamRecords', { "teamRecordDetails": JSON.stringify(options) },
        {
            onSuccess: function(result)
            {
                _this.returnData = JSON.parse(result);
                _this.teamMembers = _this.returnData.membersList;
                _this._internalShow();
                _this._renderGrid();
                _this._renderSideBar();
                
            },//onSuccess
            onFailure: function(error)
            {
                var winTitle = "Error";
                var errMessage = error.faultstring;//"An Unexpected Error Occured, please send the details to your Administrator";
                
                winTitle = error.faultstring.match('You do not belong to any teams') ? "Warning" : winTitle;
                errMessage = error.faultstring.match('You do not belong to any teams') ? _this.config.noTeamMessage : errMessage;
                
                ACEConfirm.show
                (
                    errMessage,
                    winTitle,
                    [ "Ok"],
                    function(result)
                    {
                        _this.close();                   
                    }//function (result)
                );
            }//onFailure
        });//sforce.apex.execute 

    };//TeamManagerForm.prototype.init = function()
    
    // Initialises the form override the createChildren function in Form.js
    TeamManagerForm.prototype._createChildren = function()
    {
        _this.sidebar = $('<aside class="teams-sideBar"></aside>').appendTo(this.parent);
        _this.mainWindow = $('<div class = "mainWindow"></div>').appendTo(this.parent);
        
        var teamNameHeader = $('<p id="teamNameHeader" class="teamNameHeader"></p>').appendTo( _this.mainWindow );//
        
        if (currentUserId == ACE.Salesforce.loggedInUserId)
        {
            _this.title(this.config.headerTitle);
        }
        else
        {
            _this.title(this.config.headerTitle);
            // DOM MANIPULATION - since AbstractDialog and jquery dialog don't support putting html tags inside the title. 
            $("div.ui-dialog-titlebar > span.ui-dialog-title").append("<span id='behalfOfDiv' style='background-color: orange; position:absolute; left:" + ($( window ).width()/2 - 150)+ "px;'>" + "Logging on behalf of " +  _this.config.userName + "</span>");
        }

        // we might have to update the title LATER. 

        // _this.title(this.config.headerTitle);
        
        //Create the lookup for employee names first        
        var empConfig = this.config.employeeLookup;
        
        var empLookup = {
            id                  : empConfig.id,
            field               : empConfig.dataField,
            width               : empConfig.width,
            name                : empConfig.label,
            placeHolder         : empConfig.placeHolder,
            sortable            : false,
            fieldType           : "STRING",
            editable            : true,
            disabled            : false,
            maintainSelectedItem: false,
            additionalConfig    : empConfig.additionalConfig,
            selectCallback      : _this.addRowToMembersList
        };        
        
        _this.appendedEmployee = {};
        _this.employeeLookup = 
            ACE.TeamManagerFormItemFactory
            .inputMap['LOOKUP']
            .call(_this, _this.mainWindow, null, _this.appendedEmployee, empLookup);
        
        //_this.
        
    };//TeamManagerForm.prototype._createChildren
    
    //_this._internalShow();
    //_this._renderGrid();
    TeamManagerForm.prototype._renderGrid = function()
    {
        _this.membersContainer = $('<div class="membersContainer"></div>').appendTo(this.mainWindow);
        
        _this.isDelegateAdmin = _this.config.isAdmin;
        
        _this.membersGrid = new ACEDataGrid(this.membersContainer , 
        {  
            autoHeight              : true, 
            forceFitColumns         : true, 
            explicitInitialization  : true,
            editable                : true,																					
            headerRowHeight         : 40,
            autoEdit                : false,
            //handleDelete            : true,
            maxNumberOfRows         : 10    
        });
        
        _this.membersGrid.setColumns(this.getColumns(),
        
            {
                "DeleteItemRenderer": new ItemRenderer(function(row, cell, value, columnDef, dataContext)
                {
                    var hidden = '';
                    
                    if(!_this.isAdmin)
                    {
                       hidden = "hidden";
                    }
                    
                    
                    var buttonDiv;
                    
                    if (value)
                    {                     
                        buttonDiv =  '<div '+ hidden +' data-type="undoButton" class="undoButton"></div>';
                    }else{
                        buttonDiv =  '<div '+ hidden +' data-type="deleteButton" class="deleteButton"></div>';
                    }
                    
                    
                    
                    return buttonDiv;
                    
                })
            } 
        ); 
        _this.membersGrid.grid.init();
        
        $(window).resize(function() {
            _this.membersGrid.grid.resizeCanvas();
        });
                
        $.each(_this.teamMembers, function(index, item)
        {
            //acedatagrid will not show the record due to the uppercasing in "Id". Making a copy of the Id to a lower case "id"
            if(item.id == null || item.id == undefined )
            {
              item.id = item.Id;
              delete item.Id;
            }
        });
        
        _this.membersGrid.addItems( _this.teamMembers );

        //Render the Ace grid and the side bar using the information we recieved from SFDC
    };//TeamManagerForm.prototype._renderGrid
    
    TeamManagerForm.prototype.addRowToMembersList = function()
    {
        _this.i = (_this.i == null) ? 0 : _this.i + 1;
        
        var item = 
        {
            id                      : 'newRow' + _this.i,
            T1C_Base__Employee__r   : _this.appendedEmployee.T1C_Base__Employee__c,
            T1C_Base__Team__c       : _this.selectedTeam,
            T1C_Base__Inactive__c   : false,
            T1C_Base__Employee__c   : _this.appendedEmployee.T1C_Base__Employee__c.Id
        }

        var hasDupe = false;
        $.each(_this.membersGrid.dataProvider.getItems(), function(index, teamMember)
        {
            hasDupe = hasDupe || (teamMember.T1C_Base__Team__c == item.T1C_Base__Team__c && teamMember.T1C_Base__Employee__c == item.T1C_Base__Employee__c);
        });
        
        if(!hasDupe)
        {
            _this.membersGrid.addItems(item);
        }
        else
        {
            ACE.LoadingIndicator.show('This Employee is already in the selected Team');
            window.setTimeout(function()
            {
                ACE.LoadingIndicator.hide();
            },2000);
        }
        
    }
    
    TeamManagerForm.prototype._renderSideBar = function()
    {
        //Render the sidebar using the team names from the results. The sidebar needs to be filterable so we add a slick grid to it
        
        _this.teamSet = _this.returnData.teamsList;
        _this.managedTeams = {};
        _this.unmanagedTeams = {};
        $.each(_this.teamMembers, function(index, item)
        {
            //Store it by ID. match the Role as well. Process only this user within the team members list
            if(item.T1C_Base__Employee__c === _this.config.curEmployeeId || _this.config.isAdmin)
            {
                var role = item.T1C_Base__Role__c ? item.T1C_Base__Role__c : "";
                var curItem = {
                    'Id'    : item.T1C_Base__Team__r.Id,
                    'Name'  : item.T1C_Base__Team__r.Name,
                    'Role'  : role
                };
                
                if(_this.config.isAdmin != true)
                {
                    var curSet = ( role != "" && _this.config.managingRoles.match(role) ) ? _this.managedTeams : _this.unmanagedTeams;
                    curSet[curItem.Id] = curItem;
                }
            }   
        });//$.each
        
        var asideHeaderThree = $('<h3 class="sideBar-title">Teams</h3>').appendTo(_this.sidebar);
        
        //If not a delegated admin, sort the teams by managed and unmanaged
        var label  = _this.config.isAdmin != true ? "Team Manager" : "";//No label if we're managing all teams
        var managedSet = _this.config.isAdmin != true ? _this.managedTeams : _this.teamSet;
        var teamFilter = $('<LA-Input id="ManagedTeamsFilter" class="sidebarTeamDiv" placeHolder="' + _this.config.filterPlaceHolder + '"></input>').appendTo(_this.sidebar);

        _this.managedTeamDiv = $('<div id="ManagedTeams" class="sidebarTeamDiv"><p class="managedListTitle">'+ label + '</p></div>').appendTo(_this.sidebar);
        
        //add autocomplete="off" to the internal input in the raw LA-Input
        var input = teamFilter.find('input');
        input.attr("autocomplete","off");
        
        var managedList = $('<ul id="managedList"></ul>').appendTo(_this.managedTeamDiv);
        //iterate through the teams and create buttons and divs for each
        $.each(managedSet, function(ind, curItem)
        {
            var managedTeamItem = $('<li id="' + curItem.Id + '" class="sidebarTeamItem">' + curItem.Name + '</li>').appendTo(managedList);
            //Select the first one
            if(!_this.selectedTeam) {
                _this.selectedTeam = curItem.Id;
                _this.selectedTeamName = curItem.Name;
                _this.isAdmin = true;
                _this.selectedDiv = managedTeamItem;
                managedTeamItem.addClass("sidebarTeamItemClicked");
            }
            _this.addSideBarItemListeners(managedTeamItem, true, curItem);
        });
        
        var unmanagedList;//Keep a reference
        if(_this.config.isAdmin != true && Object.keys(_this.unmanagedTeams).length > 0)
        {
            _this.unmanagedTeamDiv = $('<div id="UnmanagedTeams" class="sidebarTeamDiv"><p class="unmanagedListTitle">Team Member</p></div>').appendTo(_this.sidebar);
            unmanagedList = $('<ul id="unmanagedList"></ul>').appendTo(_this.unmanagedTeamDiv);
            $.each(_this.unmanagedTeams, function(ind, curItem)
            {
                var unmanagedTeamItem = $('<li id="' + curItem.Id + '" class="sidebarTeamItem">' + curItem.Name + '</li>').appendTo(unmanagedList);

                //Select the first one if we didnt get one from the managed teams. This means that we have no Managed Teams
                if(!_this.selectedTeam) {
                    _this.selectedTeam = curItem.Id;
                    _this.selectedTeamName = curItem.Name;
                    _this.isAdmin = false;
                    _this.selectedDiv = unmanagedTeamItem;
                    $(_this.employeeLookup)[0].style.display = 'none';
                    $(_this.employeeLookup).attr('disabled','true');
                    _this.managedTeamDiv[0].style.display = "none";
                    unmanagedTeamItem.addClass("sidebarTeamItemClicked");
                }
                
                _this.addSideBarItemListeners(unmanagedTeamItem, false, curItem);
                
            });
        }

        _this.membersGrid.dataProvider.setFilter
        (
            function(item, args)
            {
                return item.T1C_Base__Team__c == _this.selectedTeam;                
            }
        
        );
        
        _this.membersGrid.grid.onBeforeEditCell.subscribe( 
        function(e,args)
        {
            return _this.isAdmin && !JSON.parse(args.column.disabled);
        });
        
        
        _this.membersGrid.grid.onKeyDown.subscribe(function(e, args)
        {			
            //Delete Key... TODO: Add a keyboard constant if jquery doesn't have one
            if (e.which == 46 && _this.isAdmin)
            {
                ACEConfirm.show
                (
                    _this.config.multiDeleteMessage,
                    "Confirm",
                    [ "Ok","Cancel"],
                    function(result)
                    {
                        if(result == 'Ok')
                        {
                            
                            _this.membersGrid.dataProvider.beginUpdate();
                            $.each(_this.membersGrid.grid.getSelectedRows(), function (index, item)
                            {
                                var curItem = _this.membersGrid.grid.getDataItem(item);
                                //Set the items to inactive, if they are new then just delete them
                                curItem.T1C_Base__Inactive__c = true;
                                
                                var matches = curItem.id.match('newRow');
                                
                                if(matches)
                                {
                                    _this.membersGrid.dataProvider.deleteItem(curItem.id);
                                }else
                                {
                                    _this.membersGrid.dataProvider.updateItem(curItem.id, curItem);
                                }
                            });
                            _this.membersGrid.dataProvider.endUpdate();
                        }//else do nothing
                    }//function (result)
                );
                
                
                
            }
        });
        
        _this.membersGrid.on("itemClick", function(e)
        {
            var target = $(e.event.target);
            var dataType = target.attr("data-type");
            if( _this.isAdmin && (dataType == 'deleteButton' || dataType == 'undoButton'))
            {
                var matches = e.data.id.match('newRow');
                if( matches )
                {
                    _this.membersGrid.dataProvider.deleteItem(e.data.id);
                }
                else
                {
                    e.data.T1C_Base__Inactive__c = !e.data.T1C_Base__Inactive__c;
                }
                _this.membersGrid.dataProvider.updateItem(e.data.id, e.data);
            }
            else
            {
                $(e.event.target).focusin();
            }
            //_this.membersGrid.dataProvider.reSort();
        });
        
        _this.membersGrid.grid.onActiveCellChanged.subscribe(function(e,args)
        {
            args.grid.editActiveCell();
        });
        
        //Filter listeners
        teamFilter.on("keyup click change", 
        function(e,args)
        {
            //on keyup, filter both lists if not undefined
            managedLi = managedList.children();
            var filter = e.target.value.toUpperCase();
            // Loop through all list items, and hide those who don't match the search query
            var hideDiv = true;//Used to detect if we shoul Hide this div
            for (i = 0; i < managedLi.length; i++) {
                a = managedLi[i];
                txtValue = a.textContent || a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    managedLi[i].style.display = "";
                    hideDiv = false;
                } else {
                    managedLi[i].style.display = "none";
                }
            }
            if(hideDiv)
            {
                _this.managedTeamDiv[0].style.display = "none";
            }else
            {
                _this.managedTeamDiv[0].style.display = "";
            }
            
            if(unmanagedList)
            {
                unmanagedLi = unmanagedList.children();
                var hideunmanagedDiv = true;//Used to detect if we shoul Hide this div
                // Loop through all list items, and hide those who don't match the search query
                for (i = 0; i < unmanagedLi.length; i++) {
                    a = unmanagedLi[i];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        unmanagedLi[i].style.display = "";
                        hideunmanagedDiv = false;
                    } else {
                        unmanagedLi[i].style.display = "none";
                    }
                }
                
                if(hideunmanagedDiv)
                {
                    _this.unmanagedTeamDiv[0].style.display = "none";
                }else
                {
                    _this.unmanagedTeamDiv[0].style.display = "";
                }
                
            }//if unmanagedList
            
        });
        
        $('#teamNameHeader')[0].innerHTML = _this.selectedTeamName + ' (' +_this.membersGrid.dataProvider.getLength() + ')';
        
        _this.membersGrid.grid.invalidateAllRows();
        _this.membersGrid.grid.resizeCanvas();
    };//TeamManagerForm.prototype._renderSideBar   
    
    TeamManagerForm.prototype.addSideBarItemListeners = function(component, managed, filterItem) 
    {
        component.on("mouseenter", function(e)
        {
            $(this).addClass("sidebarTeamItemHover");            
        });
        
        component.on("mouseleave", function(e)
        {
            $(this).removeClass("sidebarTeamItemHover");
        });
        
        //Toggle the filter depending on class presence
        component.on("click", function(e)
        {
            _this.membersGrid.clearSelectedRows();
            
            _this.selectedDiv.removeClass("sidebarTeamItemClicked");
            _this.selectedDiv = $(this).addClass("sidebarTeamItemClicked");
            
            _this.isAdmin = managed || _this.isDelegateAdmin;
            
            if(!_this.isAdmin)
            {
                $(_this.employeeLookup)[0].style.display = 'none';
                $(_this.employeeLookup).attr('disabled','true');
            }else
            {
                $(_this.employeeLookup)[0].style.display = '';
                $(_this.employeeLookup).removeAttr('disabled');
            }
            
            _this.selectedTeam = filterItem.Id;
            _this.selectedTeamName = filterItem.Name;
            
            //refresh grid dataprovider
            _this.membersGrid.dataProvider.refresh();
            
            $('#teamNameHeader')[0].innerHTML = _this.selectedTeamName + ' (' +_this.membersGrid.dataProvider.getLength() + ')';
            
            //filterItem
        });
    }//TeamManagerForm.prototype.addSideBarItemListeners
    
    TeamManagerForm.prototype.getColumns = function ()
    { 
        var columns = [];  
        if(this.config.gridColumns.length > 0)
        {

            var datePickerFormat = function(row,cell,value,columnDef,dataContext)
            {  

                if(value === '')
                {
                  return "<span class='fakeDatePicker'></span>";
                }
                var dateValue = new Date(value);
                var dateString = '';
                var correctedMilliTime = dateValue.getTime()  + (dateValue.getTimezoneOffset() * 60000);
                if(correctedMilliTime > 0)
                {
                    dateValue = new Date(correctedMilliTime);
                    dateString = dateValue.toLocaleDateString();
                }

                return "<span class='fakeDatePicker'>" + dateString + "</span>";
            }

            var phoneNumberFormatter= function(row,cell,value,columnDef,dataContext)
            {
              var phoneMatch = value.match(/\d{10}/);
              if(phoneMatch != null)
              {
                  console.log("Changing number " + value);
                  value = value.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
                  console.log("Number Changed to " + value);
                  dataContext[columnDef.field.toString()] = value;
              }
              return value;
            }
          
            var CheckboxBOX = function (row, cell, value, columnDef, dataContext) 
            {
                var checkbox;
                if(value == true || value == 'true')
                {
                  checkbox = '<input type="checkbox" checked>';
                }
                else
                {
                  checkbox = '<input type="checkbox">';
                }

                return checkbox;
            }
            
            var pickListFormat = function (row, cell, value, columnDef, dataContext) 
            {
                var val = value.length > 0 ? value : columnDef.defaultValue;

                var adminStyle = !_this.isAdmin ? "menuButtonHidden" : ""
                
                return '<span class="fakeACEMenuButton ' + adminStyle + '" title="' + val + '">' + val + '</div>';
            }
            
            var removeButtonFormat = function(row,cell,value,columnDef,dataContext)
            {  
                var deleteButton = '<button class="deleteButton"></button>';
                var undoButton = '<button class="undoButton"></button>'
                var retVal = deleteButton;
                if(value){ retVal = undoButton; }
                
                if(!_this.isAdmin)
                {
                    $(retVal)[0].style.display = "none"
                }
                
                return retVal;
            }
            
            var removeButton =  
            {
                id          : "removeButton", 
                name        : "", 
                field       : "T1C_Base__Inactive__c",
                //editor      : Slick.Editors.Checkbox,
                //formatter   : removeButtonFormat,
                disabled    : false,
                formatter   : "DeleteItemRenderer",
                width       : 24,
                maxWidth    : 30
            };

            columns.push(removeButton);
            
            $.each(this.config.gridColumns, function(index, item)
            {
                var o = {};
                if(item.fieldType == "LOOKUP")
                {
                  o = {
                    id              : item.id,
                    field           : item.dataField,
                    width           : item.width,
                    name            : item.label,
                    sortable        : false,
                    fieldType       : "STRING",
                    placeHolder     : item.placeHolder,    
                    editable        : !JSON.parse(item.disabled.toString()),
                    additionalConfig: item.additionalConfig,
                    disabled        : item.disabled.toString().toLowerCase(),
                    editor          : Slick.Editors.AutoComplete
                  };                  
                }
                else if(item.fieldType == "PICKLIST")
                {
                  o = {
                    id                  : item.id,
                    field               : item.dataField,
                    minwidth            : item.width,
                    name                : item.label,
                    sortable            : false,
                    fieldType           : "STRING",
                    editorDataProvider  : item.additionalConfig.dataProvider,
                    editable            : !JSON.parse(item.disabled.toString()),
                    disabled            : item.disabled.toString().toLowerCase(),
                    formatter           : pickListFormat,
                    defaultValue        : item.defaultValue,
                    cssClass            : "menuButtonColumn",
                    editor              : Slick.Editors.ACEMenuButton,
                    editorOptions : //New in 1.21
                        {
                            defaultValue : item.defaultValue,
                            dataField    : "data",
                            getValOnly   : true
                        }
                    };
                } 
               else if(item.fieldType == "CHECKBOX")
                {
                  o = {
                    id              : item.id,
                    field           : item.dataField,
                    width           : item.width,
                    name            : item.label,
                    sortable        : false,
                    fieldType       : "CHECKBOX",
                    editable        : !JSON.parse(item.disabled.toString()),
                    disabled        : item.disabled.toString().toLowerCase(),
                    formatter       : Slick.Editors.Checkbox
                };
               }
               else if(item.fieldType == "DATE")
               {
                o = {
                  id                : item.id,
                  field             : item.dataField,
                  width             : item.width,
                  name              :item.label,
                  selectable        : true,
                  sortable          : false,
                  focusable         : true,
                  fieldType         : "DATE",
                  editable          : !JSON.parse(item.disabled.toString()),
                  formatter         : datePickerFormat,
                  disabled          : item.disabled.toString().toLowerCase(),
                  editor            : Slick.Editors.DateEditor
                };
               }
               else
               {
                  o = {
                    id              : item.id,
                    field           : item.dataField,
                    width           : item.width,
                    name            : item.label,
                    sortable        : false,
                    fieldType       : "STRING",
                    placeHolder     : item.placeHolder,
                    editable        : !JSON.parse(item.disabled.toString()),
                    /*formatter       : plainTextFormatter,*/
                    disabled        : item.disabled.toString().toLowerCase(),
                    editor          : Slick.Editors.Text  
                };
               }
                o.color="#FF9968";
                columns.push(o);

          });
          
      }//if(this.config.fieldFormFields.length > 0)

      return columns;
    };//TeamManagerForm.prototype.getColumns
    
    TeamManagerForm.prototype.close = function()
	{
	   //TODO: pop close dialog only when changes occur
		window.close();
	}
    
    TeamManagerForm.prototype.save = function(closeAfter)
	{
        var items = _this.membersGrid.dataProvider.getItems();
        
        ACE.LoadingIndicator.show('Saving');
        
        sforce.apex.execute('TeamManagerController', 'saveTeamRecords', { "teamRecordDetails": JSON.stringify(items) },
        {
            onSuccess: function(result)
            {
                var successMessage = _this.config.saveMessageLabel ? _this.config.saveMessageLabel : "Success";
                
                ACE.LoadingIndicator.hide();
                ACE.LoadingIndicator.show(successMessage);
                window.setTimeout(function()
                {
                    ACE.LoadingIndicator.hide();
                    window.setTimeout(function()
                    {
                        location.reload(true);
                        if(closeAfter)
                        {
                        	_this.close();
                        }
                    }, 300);
                }, 1700);
                
            },//onSuccess
            onFailure: function(error)
            {
                //Fill out Error messages. For now permission is the only one detected
                //Permission denied: error.faultcode = sf:INSUFFICIENT_ACCESS
                var errormsg = "";
                
                if ( error.faultstring.indexOf("INSUFFICIENT_ACCESS") > -1)
                {
                    errormsg = 
                        "Insufficient Access error for this User on the Team object";
                }else if ( error.faultstring.indexOf("FIELD_CUSTOM") > -1)
                {
                    var pattern = /.*FIELD_CUSTOM_VALIDATION_EXCEPTION,\s(.*):\s\[[\s\S]*/
                    errormsg = error.faultstring.replace(pattern, /$1/).replace(/\//g,"");
                }                
                else if ( error.faultstring.indexOf("invalid") > -1)
                {
                    //var pattern = /.*INVALID.*?,.*:\s(.*):\s\[[\s\S]*/
                    //errormsg = "Invalid input: " + error.faultstring.replace(pattern, /$1/).replace(/\//g,"");
                    var typePattern = /\sinvalid\s(.*?):/
                    var type = error.faultstring.match(typePattern);
                    errormsg = "Please enter a valid " + type[1];
                    
                }else
                {
                    errormsg = "An Unexpected Error Occured. Please send the details to your administrator";
                }

                ACE.FaultHandlerDialog.show
                ({
                    title:   "Warning",
                    message: errormsg, 
                    error:   error.faultstring
                });

            }//onFailure
		});//sforce.apex.execute upsertToSFDC
        
        
    };//TeamManagerForm.prototype.save
    $.extend(true, window,
    {
        "ACE":
        {
            "TeamManagerForm": new TeamManagerForm()
        }
    });	
    
//# sourceURL=TeamManagerForm.js
})(jQuery);