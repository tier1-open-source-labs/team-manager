//=============================================================================
/**
 * Name: TeamManagerFormItemFactory.js
 * Description: Factory for the Non-grid inputs in the Affiliations form
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{   
    
	var inputMap;
	
	//SVGs
	//Define SVGs
    var undoIcon = '<svg class="undo" style="width:24px;height:24px" viewBox="0 0 24 24">' +
    '<path fill="#797979" d="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z" />'+
    '</svg>';
    
    var trashIcon = '<svg class="trash" style="width:20px;height:20px" viewBox="0 0 24 24">'+
    '<path fill="#797979" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />'+
    '</svg>';  
	
    //The following are all expected to have the signature: function (component, label, val, options) since they will all go into the map    
    /*
        Lookup Input
    */
	var createLookupInput = function (component, label, val, options) 
    {

		var dataField = options.field.split(".");
		var prop;
        
        if(val){
            var i = 1;
            prop = val[ dataField[0] ];
            if(prop)
            {
                for(i ; i < dataField.length ; i++)
                {
                    if(!prop[ dataField[i] ]){break;}
                    prop = prop[ dataField[i] ];//go further into the object structure
                }
            }
        }
		var valString = prop ? prop : "";
        var placeHolderString = options.placeHolder ? options.placeHolder : "";
        var lookupItem = $('<div class="editControlContainer" style="width:60%"/>').appendTo(component);
        if(label && label.length > 0){$('<label class="itemLabel">' + label + '</label>').appendTo(lookupItem);}
        var lookup = $p('<input placeHolder="' + placeHolderString + '" value="' + valString + '"></input>').appendTo(lookupItem);

        if(options.disabled === 'true'){$(lookup).attr('disabled','true');}
        
        setTimeout(function()
        {
            var autoComplete = 
                new SearchAutoComplete($(lookup),
        		{
	                mode: 					"CUSTOM:" + options.additionalConfig.id,
	            	config: 				options.additionalConfig,
	                coverageOnly: 			false,
	                maintainSelectedItem:	options.maintainSelectedItem
        		});	
            
            autoComplete.set("select", function(item)
			{
            	var relationString = dataField[0].replace("__r$", "__c");
            	val[relationString] = item;
                
            	$(lookup)[0].setAttribute("value", item[ dataField[i] ]);
                
                if(options.selectCallback)
                {
                    options.selectCallback.call(this, val);
                }
                
			});
            //autoComplete
            
        }
        ,100);
        
        return lookup;
        
    }//Create Lookup Input

    /*
        Picklist Input
    */
    var createPickListInput = function (component, label, val, options) 
    {
    
    	var datafield = options.dataField
        var lookupItem = $('<div class="componentContainer subSectionItem" style="width:80%"/>').appendTo(component);
        if(label && label.length > 0){$('<label class="itemLabel">' + label + '</label>').appendTo(lookupItem);}
    
        if(options.additionalConfig)
        {
        	var picklistValues = options.additionalConfig.dataProvider;
        }
        
        var menuButton = new ACEMenuButton($(lookupItem),
            {
        		dataProvider : picklistValues
        	}
        );
        
        if(val[datafield])
        {
        	menuButton.setSelectedItem( val[datafield], false );
        } 
        else
        {
        	menuButton.clearSelectedItem();
        }
        
        menuButton.on("click", function(event)
		{
        	val[datafield] = event.item;
		});
        
    }
    
    /*
        Text Input
    */
    var createTextInput = function (component, label, val, options) 
    {
        var lookupItem = $('<div class="componentContainer subSectionItem" style="width:80%"/>').appendTo(component);
        if(label && label.length > 0){$('<label class="itemLabel">' + label + '</label>').appendTo(lookupItem);}
        
        var datafield = options.dataField
        
        var curVal = val[datafield] ? val[datafield] : "";
        
        var thisTextBox = $('<input class="ui-autocomplete-input" value="' + curVal + '"type="text"/>').appendTo(lookupItem);
        
        $(thisTextBox).change( [val, datafield] ,function(event)
		{
        	var object = event.data[0];
        	var field = event.data[1];
        	object[field] =  this.value;
		});
        
    }
    
    /*
        Checkbox Input
    */
    var createCheckBoxInput = function (component, label, val, options) 
    {
    	var datafield = options.dataField
    	
        var lookupItem = $('<div class="componentContainer subSectionItem" style="width:80%"/>').appendTo(component);
        if(label && label.length > 0){$('<label class="itemLabel">' + label + '</label>').appendTo(lookupItem);}
        
        var thisCheckBox = $('<input type="checkbox"></input>').appendTo(lookupItem);
        
        $(thisCheckBox)[0].checked = val[datafield] === "true";
        
        $(thisCheckBox).click(function() 
		{
        	val[datafield] = $(thisCheckBox)[0].checked;
		});
        
    }
    
    /*
        Date Input
    */   
    var createDateInput = function (component, label, val, options) 
    {
    	var datafield = options.dataField
    	
        var lookupItem = $('<div class="componentContainer subSectionItem" style="width:80%"/>').appendTo(component);
        if(label && label.length > 0){$('<label class="itemLabel">' + label + '</label>').appendTo(lookupItem);}
        
        var datePicker = $('<input Id="Test"/>').datepicker();
		
		$(datePicker).appendTo(lookupItem);
		$(datePicker).val( val[datafield] );
		
		$(datePicker).on('change', function() 
		{
			val[datafield] = $(datePicker).val();
		});
		
    }
    
    //"Constructor"
    function TeamManagerFormItemFactory()
    {
    	this.inputMap = 
        {
            'LOOKUP'    : createLookupInput,
            'PICKLIST'  : createPickListInput,
            'STRING'    : createTextInput,
            'TEXT'    	: createTextInput,
            'CHECKBOX'  : createCheckBoxInput,
            /*'DELETE'    : createDeleteToggle,*/
            'DATE'      : createDateInput
	    };
    }
    
    $.extend(true, window,
		{
			"ACE":
			{
				"TeamManagerFormItemFactory": new TeamManagerFormItemFactory()
			}
		}
    );	
//# sourceURL=TeamManagerFormItemFactory.js
})(jQuery);